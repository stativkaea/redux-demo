/*global require*/
/*global module*/
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';
import createSagaMiddleware from 'redux-saga'
import {loadingBarMiddleware} from "react-redux-loading-bar";
import reducer from '../reducers/index'
import mySaga from "./middlewares/sagas";

export const getStore = () => {
  const sagaMiddleware = createSagaMiddleware();

  const store = createStore(
    reducer,
    composeWithDevTools(
      applyMiddleware(loadingBarMiddleware(), sagaMiddleware),
    ),
  );

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers/index', () => {
      const nextRootReducer = require('../reducers/index').default;
      store.replaceReducer(nextRootReducer);
    });
  }

  sagaMiddleware.run(mySaga);

  return store;
};
