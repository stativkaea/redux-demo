// @flow
import React from "react";
import {Button, Card, Icon, Image} from "semantic-ui-react";
import "./ItemCard.css";

type itemCardProps = {
    isInWatchLater: boolean,
    id: number,
    imageUrl: string,
    original_name: string,
    original_title: string,
    release_date: string,
    first_air_date: string,
    overview: string,
    vote_average: number,
    markAsWatchLater: Function,
    unMarkAsWatchLater: Function,
};

export const ItemCard = ({
   isInWatchLater,
   id,
   imageUrl,
   original_name,
   original_title,
   release_date,
   first_air_date,
   overview,
   vote_average,
   markAsWatchLater,
   unMarkAsWatchLater,
 }: itemCardProps) => (
  <Card className="card-wrapper">
    <Card.Content>
      <Image floated='right' size='mini' src={imageUrl}/>
      <Card.Header>
        {original_name || original_title}
      </Card.Header>
      <Card.Meta>
        <span className='date'>
          {release_date || first_air_date}
        </span>
      </Card.Meta>
      <Card.Description className="truncated">
        {overview}
      </Card.Description>
    </Card.Content>
    <Card.Content className="buttons" extra>
      <span>
        <Icon name='star'/>
        {vote_average || 'n/a'}
      </span>
      <Button
        onClick={() => isInWatchLater ? unMarkAsWatchLater(id) : markAsWatchLater(id)}
        color={isInWatchLater ? 'red' : 'green'}
        className="watch-later"
        content={(isInWatchLater ? 'Remove from' : 'Add to') + ' my list'}
        icon={isInWatchLater ? 'delete' : 'save'}
        labelPosition="right"/>
    </Card.Content>
  </Card>
);
