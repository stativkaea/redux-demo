import { List } from 'immutable';
import {SEARCH_SUCCEEDED} from '../actions/search';

export const searchResults = (state = List(), action) => {
  switch (action.type) {
      case SEARCH_SUCCEEDED:
      return List.of(...action.searchResults);
    default:
      return state;
  }
};
