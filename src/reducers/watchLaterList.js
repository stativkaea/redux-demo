import { List } from 'immutable';
import {ADD_TO_WATCH_LATER, REMOVE_FROM_WATCH_LATER} from "../actions/watchLater";

export const watchLaterList = (state = List(), action) => {
  switch (action.type) {
    case ADD_TO_WATCH_LATER:
      return state.push(action.movie);

    case REMOVE_FROM_WATCH_LATER:
      return state.filter(item => item.id !== action.id);

    default:
      return state;
  }
};
