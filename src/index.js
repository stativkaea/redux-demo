/*global document*/
/*global module*/
/*global require*/
/*global setTimeout*/
/*global console*/

import React from 'react';
import { render } from 'react-dom';
import {Provider} from "react-redux";
import registerServiceWorker from './registerServiceWorker';
import {getStore} from "./store/index";
import 'semantic-ui-css/semantic.min.css';
import './index.css';

const store = getStore();
const rootEl = document.getElementById('root');

// eslint-disable-next-line immutable/no-let
let renderApp = () => {
  const AppContainer = require('./containers/App').default;

  render(
    <Provider store={store}>
      <AppContainer/>
    </Provider>,
    rootEl,
  )
};

// HMR for AppContainer and its dependencies
if(module.hot) {
  const renderInDevMode = renderApp;
  const renderError = (error) => {
    const RedBox = require('redbox-react').default;
    render(
      <RedBox error={error} />,
      rootEl,
    );
  };

  // handle errors in devMode
  renderApp = () => {
    try {
      renderInDevMode();
    }
    catch(error) {
      // eslint-disable-next-line no-console
      console.error(error);
      renderError(error);
    }
  };

  // re-import AppContainer on changes
  module.hot.accept('./containers/App', () => {
    setTimeout(renderApp);
  });
}

renderApp();

registerServiceWorker();
