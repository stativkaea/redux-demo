import {combineReducers} from "redux";
import {searchResults} from "./searchResults";
import {watchLaterList} from "./watchLaterList";
import {loadingBarReducer} from 'react-redux-loading-bar';

export default combineReducers({
  loadingBar: loadingBarReducer,
  searchResults,
  watchLaterList,
});
