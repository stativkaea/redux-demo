import {createSelector} from "reselect";

export const getWatchLaterList = createSelector(state => state.watchLaterList.toArray(), state => state);
