export const SEARCH = 'SEARCH';

export const SEARCH_SUCCEEDED = 'SEARCH_SUCCEEDED';

export const SEARCH_FAILED = 'SEARCH_FAILED';

export const search = searchQuery => ({ type: SEARCH, searchQuery });

export const searchSucceed = searchResults => ({ type: SEARCH_SUCCEEDED, searchResults });

export const searchFail = error => ({ type: SEARCH_FAILED, error });
