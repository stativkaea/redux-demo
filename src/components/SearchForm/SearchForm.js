import React, {Component} from "react";
import PropTypes from "prop-types";
import {Form} from "semantic-ui-react";
import './SearcForm.css';

export class SearchForm extends Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const value = event.target.value;
    this.setState(() => ({value}));
  }

  handleSubmit(event) {
    if (this.state.value.length > 1) {
      this.props.handleSubmit(this.state.value);
      this.setState(() => ({ value: '' }));
    }
    event.preventDefault();
  }

  render() {
    return (
      <Form className="search-form" size="huge" onSubmit={this.handleSubmit}>
        <Form.Group>
          <Form.Field inline>
            <label>Movie or TV show name:</label>
            <input placeholder='at least 2 symbols' name='name' value={this.state.value} onChange={this.handleChange} />
          </Form.Field>
          <Form.Button content='Submit'/>
        </Form.Group>
      </Form>
    );
  }
}

SearchForm.propTypes = {
  handleSubmit: PropTypes.func,
};
