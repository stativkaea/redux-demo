export const ADD_TO_WATCH_LATER = 'ADD_TO_WATCH_LATER';

export const REMOVE_FROM_WATCH_LATER = 'REMOVE_FROM_WATCH_LATER';

export const addToWatchLater = movie => ({ type: ADD_TO_WATCH_LATER, movie });

export const removeFromWatchLater = id => ({ type: REMOVE_FROM_WATCH_LATER, id });
