// @flow
import React, {Component} from "react";
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {ItemCard} from "../components/ItemCard/ItemCard";
import {SearchForm} from "../components/SearchForm/SearchForm";
import {WatchLaterList} from "../components/WatchLaterList/WatchLaterList";
import { search } from "../actions/search";
import {addToWatchLater, removeFromWatchLater} from "../actions/watchLater";
import {getSyncedSearchResults} from "../selectors/searchResults";
import Grid from "semantic-ui-react/dist/es/collections/Grid/Grid";
import "./App.css";
import {LoadingBar} from "react-redux-loading-bar";
import {getWatchLaterList} from "../selectors/watchLaterList";

class App extends Component {
  constructor(props) {
    super(props);
    this.search = this.search.bind(this);
    this.markAsWatchLater = this.markAsWatchLater.bind(this);
    this.unMarkAsWatchLater = this.unMarkAsWatchLater.bind(this);
  }

  componentDidMount() {
    this.props.search();
  }

  search(searchQuery) {
    this.props.search(searchQuery.toLowerCase().replace(/ /g,"+"));
  }

  markAsWatchLater(id) {
    const movie = this.props.searchResults.find(item => item.id === id);
    this.props.addToWatchLater(movie);
  }

  unMarkAsWatchLater(id) {
    this.props.removeFromWatchLater(id);
  }

  render() {
    return (
      <Grid className="App" columns={2}>
        <Grid.Row>
          <LoadingBar loading={this.props.loadingBar} />
        </Grid.Row>
        <Grid.Row>
          <Grid.Column className="left-column" width="3">
            <WatchLaterList watchLaterItems={this.props.watchLaterList} removeHandler={this.unMarkAsWatchLater}/>
          </Grid.Column>

          <Grid.Column width="13">
            <SearchForm handleSubmit={this.search}/>
            {this.props.searchResults && (
              <div className="cards">
                {this.props.searchResults.length
                  ? this.props.searchResults.map(res => (
                    <ItemCard
                      key={res.id}
                      markAsWatchLater={this.markAsWatchLater}
                      unMarkAsWatchLater={this.unMarkAsWatchLater}
                      {...res}
                    />))
                  : <p>No results...</p>}
              </div>
            )}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

App.propTypes = {
  searchResults: PropTypes.array.isRequired,
  watchLaterList: PropTypes.array.isRequired,
  search: PropTypes.func.isRequired,
  loadingBar: PropTypes.number.isRequired,
  addToWatchLater: PropTypes.func.isRequired,
  removeFromWatchLater: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  loadingBar: state.loadingBar,
  searchResults: getSyncedSearchResults(state),
  watchLaterList: getWatchLaterList(state),
});

const mapDispatchToProps = dispatch => ({
  search(searchQuery) {
    dispatch(search(searchQuery));
  },
  addToWatchLater(movie) {
    dispatch(addToWatchLater(movie));
  },
  removeFromWatchLater(id) {
    dispatch(removeFromWatchLater(id));
  },
});

const AppContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);

export default AppContainer;
