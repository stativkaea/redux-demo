// @flow
/* globals fetch */
import dummy from "./dummy.png";

export const fetchMovies = searchQuery => {
  const restPrefix = 'https://api.themoviedb.org/3';
  const apiKey = 'api_key=d272326e467344029e68e3c4ff0b4059';

  const url = searchQuery
    ? `${restPrefix}/search/multi?${apiKey}&language=en-US&query=${searchQuery.toLowerCase()}`
    : `${restPrefix}/discover/movie?${apiKey}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1`;

  return fetch(url)
    .then(res => res.json())
    .then(res => res.results
      .slice(0, 12)
      .filter(res => res.original_name || res.original_title)
      .map((res: { poster_path: string }) => {
        return { ...res, imageUrl: res.poster_path ? `https://image.tmdb.org/t/p/w90_and_h134_bestv2/${res.poster_path}` : dummy };
      }))
};
