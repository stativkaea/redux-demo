import { call, put, takeLatest } from 'redux-saga/effects'
import {fetchMovies} from '../../api/api';
import {SEARCH, searchFail, searchSucceed} from "../../actions/search";
import {hideLoading, showLoading} from "react-redux-loading-bar";


function* fetchData(action) {
  try {
    yield put(showLoading());
    const searchResults = yield call(fetchMovies, action && action.searchQuery);
    yield put(searchSucceed(searchResults));
  } catch (e) {
    yield put(searchFail(e.message));
  } finally {
    yield put(hideLoading());
  }
}

function* mySaga() {
  yield takeLatest(SEARCH, fetchData);
}

export default mySaga;
