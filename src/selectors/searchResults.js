import {createSelector} from "reselect";
import {getWatchLaterList} from "./watchLaterList";

const getSearchResults = state => state.searchResults.toArray();

export const getSyncedSearchResults = createSelector(
  [getWatchLaterList, getSearchResults],
  (watchLaterList, searchResults) => searchResults.map(searchResult => {
    const found = watchLaterList.find(watchLaterItem => {
      return searchResult.id === watchLaterItem.id
    });

    return { ...searchResult, isInWatchLater: !!found };
  })
);
