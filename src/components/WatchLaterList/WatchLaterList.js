// @flow
import React from "react";
import {onlyUpdateForKeys} from "recompose";
import {Card, Feed, Icon} from "semantic-ui-react";

type watchLaterListProps = {
    watchLaterItems: Array,
    removeHandler: Function,
};

export const WatchLaterList = onlyUpdateForKeys(['watchLaterItems'])(({watchLaterItems, removeHandler}: watchLaterListProps) =>
  <Card className="watch-later">
    <Card.Content>
      <Card.Header>
        Watch Later List
      </Card.Header>
    </Card.Content>
    <Card.Content>
      <Feed>
        {watchLaterItems.length
          ? watchLaterItems.map(item => (
            <Feed.Event key={item.id}>
              <Feed.Label image={item.imageUrl}/>
              <Feed.Content>
                <Feed.Summary>
                  {item.original_name || item.original_title}
                </Feed.Summary>
                <Feed.Meta>
                  <Feed.Like onClick={() => removeHandler(item.id)}>
                    <Icon name='remove'/>
                    Remove from the list
                  </Feed.Like>
                </Feed.Meta>
              </Feed.Content>
            </Feed.Event>
          ))
          : <p>Find something to watch later</p>
        }
      </Feed>
    </Card.Content>
  </Card>
);
